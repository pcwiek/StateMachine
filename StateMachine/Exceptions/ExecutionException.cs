namespace StateMachine.Exceptions
{
    using System;

    /// <summary>
	/// Represents errors that occur during the state machine transitions
	/// </summary>
	public class ExecutionException : StateMachineException
	{
		/// <summary>
		/// Initializes a new instance of <see cref="ExecutionException"/>
		/// </summary>
		public ExecutionException()
		{
		}

		/// <summary>
		/// Initializes a new instance of <see cref="ExecutionException"/> class with specified error message.
		/// </summary>
		/// <param name="message">Error message</param>
		public ExecutionException(string message) : base(message)
		{
		}

		/// <summary>
		/// Initializes a new instance of <see cref="ExecutionException"/> class with specified error message and a reference to the inner exception that caused this exception.
		/// </summary>
		/// <param name="message">Error message</param>
		/// <param name="innerException">Inner exception that is the cause of the current exception or <c>null</c></param>
		public ExecutionException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}