﻿namespace StateMachine.Exceptions
{
    using System;

    /// <summary>
	/// Represents general state machine related error.
	/// </summary>
	public class StateMachineException : Exception
	{
		/// <summary>
		/// Initializes a new instance of <see cref="StateMachineException"/>
		/// </summary>
		public StateMachineException()
		{
		}

		/// <summary>
		/// Initializes a new instance of <see cref="StateMachineException"/> class with specified error message.
		/// </summary>
		/// <param name="message">Error message</param>
		public StateMachineException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Initializes a new instance of <see cref="StateMachineException"/> class with specified error message and a reference to the inner exception that caused this exception.
		/// </summary>
		/// <param name="message">Error message</param>
		/// <param name="innerException">Inner exception that is the cause of the current exception or <c>null</c></param>
		public StateMachineException(string message, Exception innerException)
			: base(message, innerException)
		{
		}
	}
}
