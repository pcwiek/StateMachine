namespace StateMachine.Exceptions
{
    using System;

    /// <summary>
	/// Represents errors that occurr during the initialization of the state machine
	/// </summary>
	public class InitializationException : StateMachineException
	{
		/// <summary>
		/// Creates a new instance of <see cref="InitializationException"/>
		/// </summary>
		public InitializationException()
		{
		}

		/// <summary>
		/// Initializes a new instance of <see cref="InitializationException"/> class with specified error message.
		/// </summary>
		/// <param name="message">Error message</param>
		public InitializationException(string message) : base(message)
		{
		}

		/// <summary>
		/// Initializes a new instance of <see cref="InitializationException"/> class with specified error message and a reference to the inner exception that caused this exception.
		/// </summary>
		/// <param name="message">Error message</param>
		/// <param name="innerException">Inner exception that is the cause of the current exception or <c>null</c></param>
		public InitializationException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}