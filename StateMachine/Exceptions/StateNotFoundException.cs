namespace StateMachine.Exceptions
{
    using System;

    /// <summary>
	/// Represents an error when the state is not found in the state machine
	/// </summary>
	public class StateNotFoundException : StateMachineException
	{
		/// <summary>
		/// Initializes a new instance of <see cref="StateNotFoundException"/>
		/// </summary>
		public StateNotFoundException()
		{
		}

		/// <summary>
		/// Initializes a new instance of <see cref="StateNotFoundException"/> class with specified error message.
		/// </summary>
		/// <param name="message">Error message</param>
		public StateNotFoundException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Initializes a new instance of <see cref="StateNotFoundException"/> class with specified error message and a reference to the inner exception that caused this exception.
		/// </summary>
		/// <param name="message">Error message</param>
		/// <param name="innerException">Inner exception that is the cause of the current exception or <c>null</c></param>
		public StateNotFoundException(string message, Exception innerException)
			: base(message, innerException)
		{
		}
	}
}