namespace StateMachine.Core
{
    using System;

    /// <summary>
    /// State container representation used by the <see cref="T:StateMachine.AsyncStateMachine`2"/>
    /// </summary>
    /// <typeparam name="TTrigger">Trigger type</typeparam>
    /// <typeparam name="TContext">Context type</typeparam>
    internal sealed class AsyncState<TContext, TTrigger> : MachineState<TContext, TTrigger, AsyncTrigger<TContext, TTrigger>>
    {
        public AsyncState(Type stateType) : base(stateType)
        {
        }

        /// <summary>
        /// Gets or sets the state instance exposed using <see cref="T:StateMachine.IAsyncState`1"/> interface.
        /// </summary>
        public IAsyncState<TContext> Instance { get; set; }
    }
}