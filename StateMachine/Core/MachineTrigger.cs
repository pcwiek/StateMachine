namespace StateMachine.Core
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Base class for machine trigger container
    /// </summary>
    /// <typeparam name="TContext">Context type</typeparam>
    /// <typeparam name="TTrigger">Trigger type</typeparam>
    internal abstract class MachineTrigger<TContext, TTrigger> : IEquatable<MachineTrigger<TContext, TTrigger>>
    {
        public bool Equals(MachineTrigger<TContext, TTrigger> other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return EqualityComparer<TTrigger>.Default.Equals(Value, other.Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is MachineTrigger<TContext, TTrigger> && Equals((MachineTrigger<TContext, TTrigger>) obj);
        }

        public override int GetHashCode()
        {
            return EqualityComparer<TTrigger>.Default.GetHashCode(Value);
        }

        /// <summary>
        /// Creates a new instance of <see cref="MachineTrigger{TContext,TTrigger}"/> with a given <paramref name="value"/>
        /// </summary>
        /// <param name="value">Trigger value</param>
        public MachineTrigger(TTrigger value)
        {
            Value = value;
        }

        /// <summary>
        /// Trigger value
        /// </summary>
        public TTrigger Value { get; private set; }

        /// <summary>
        /// Guard clause function
        /// </summary>
        public Func<TContext, bool> Guard { get; set; }
    }
}