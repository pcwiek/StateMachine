namespace StateMachine.Core
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Base class for the machine state container
    /// </summary>
    /// <typeparam name="TContext">Context type</typeparam>
    /// <typeparam name="TTrigger">Trigger type</typeparam>
    /// <typeparam name="TMachineTrigger">Machine trigger container type</typeparam>
    internal abstract class MachineState<TContext, TTrigger, TMachineTrigger> :
        IEquatable<MachineState<TContext, TTrigger, TMachineTrigger>>
        where TMachineTrigger : MachineTrigger<TContext, TTrigger>
    {
        public bool Equals(MachineState<TContext, TTrigger, TMachineTrigger> other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return StateType == other.StateType;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((MachineState<TContext, TTrigger, TMachineTrigger>) obj);
        }

        public override int GetHashCode()
        {
            return (StateType != null ? StateType.GetHashCode() : 0);
        }

        protected MachineState(Type stateType)
        {
            StateType = stateType;
        }

        /// <summary>
        /// Gets or sets the state type.
        /// </summary>
        public Type StateType { get; private set; }

        /// <summary>
        /// Gets or sets associated triggers.
        /// </summary>
        public IReadOnlyDictionary<TTrigger, TMachineTrigger> Triggers { get; set; }
    }
}