namespace StateMachine.Core
{
    /// <summary>
    /// Synchronous trigger
    /// </summary>
    /// <typeparam name="TContext">Context type</typeparam>
    /// <typeparam name="TTrigger">Trigger type</typeparam>
    internal sealed class Trigger<TContext, TTrigger> : MachineTrigger<TContext, TTrigger>
    {
        public Trigger(TTrigger value) : base(value)
        {
        }

        /// <summary>
        /// Target <see cref="T:StateMachine.State`2"/>
        /// </summary>
        public State<TContext, TTrigger> Target { get; set; }
    }
}