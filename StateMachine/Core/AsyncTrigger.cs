namespace StateMachine.Core
{
    /// <summary>
    /// Asynchronous trigger
    /// </summary>
    /// <typeparam name="TContext">Context type</typeparam>
    /// <typeparam name="TTrigger">Trigger type</typeparam>
    internal sealed class AsyncTrigger<TContext, TTrigger> : MachineTrigger<TContext, TTrigger>
    {
        public AsyncTrigger(TTrigger value) : base(value)
        {
        }

        /// <summary>
        /// Target <see cref="T:StateMachine.AsyncState`2"/>
        /// </summary>
        public AsyncState<TContext, TTrigger> Target { get; set; }
    }
}