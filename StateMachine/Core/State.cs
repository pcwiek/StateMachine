namespace StateMachine.Core
{
    using System;

    /// <summary>
    /// State container representation  used by the <see cref="T:StateMachine.StateMachine`2"/>
    /// </summary>
    /// <typeparam name="TTrigger">Trigger type</typeparam>
    /// <typeparam name="TContext">Context type</typeparam>
    internal sealed class State<TContext, TTrigger> : MachineState<TContext, TTrigger, Trigger<TContext, TTrigger>>
    {
        public State(Type stateType) : base(stateType)
        {
        }

        /// <summary>
        /// Gets or sets the state instance exposed using <see cref="T:StateMachine.IState`1"/> interface.
        /// </summary>
        public IState<TContext> Instance { get; set; }
    }
}