﻿namespace StateMachine
{
    /// <summary>
    /// Interface for a state in the <see cref="T:StateMachine.StateMachine`2"/>
    /// <typeparam name="TContext">Context type</typeparam>
    /// </summary>
    public interface IState<in TContext>
    {
        /// <summary>
        /// Called by the state machine when the state is entered to.
        /// </summary>
        /// <param name="context">Arbitrary context object passed from the state machine.</param>
        void OnEnter(TContext context);

        /// <summary>
        /// called by the state machine when the state is exited from.
        /// </summary>
        /// <param name="context">Arbitrary context object passed from the state machine.</param>
        void OnExit(TContext context);
    }
}
