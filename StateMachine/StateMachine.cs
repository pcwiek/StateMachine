namespace StateMachine
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Core;
    using Exceptions;

    /// <summary>
    /// A finite state machine
    /// </summary>
    /// <typeparam name="TTrigger">Trigger type</typeparam>
    /// <typeparam name="TContext">Context type</typeparam>
    public sealed partial class StateMachine<TContext, TTrigger>
    {
        private readonly Func<Type, IState<TContext>> factoryFunc;
        private IReadOnlyDictionary<Type, State<TContext, TTrigger>> states;
        private State<TContext, TTrigger> currentState;
        private readonly object _lock = new object();
        private Action<Type, TTrigger> onInvalidTrigger;

        private IState<TContext> CreateInstance(Type type)
        {
            var instance = factoryFunc(type);

            if (instance == null)
            {
                throw new StateMachineException("Null is not a valid value for instance of state type " + type);
            }

            return instance;
        }

        /// <summary>
        /// Transitions from current state to another state
        /// </summary>
        /// <param name="newState">Target state the machine should transition to</param>
        /// <param name="resume">Indicates whether the machine should 'resume' from the target state or perform a regular transition</param>
        private void Transition(State<TContext, TTrigger> newState, bool resume = false)
        {
            if (currentState != null && !resume)
            {
                currentState.Instance.OnExit(Context);
            }

            var previousState = currentState;

            if (newState != null)
            {
                newState.Instance = CreateInstance(newState.StateType);

                if (!resume)
                {
                    newState.Instance.OnEnter(Context);
                }
                currentState = newState;
                OnTransitionCompleted(new TransitionEventArgs(previousState != null ? previousState.StateType : null,
                    currentState.StateType));
            }
            else
            {
                currentState = null;
            }
        }

        private void OnTransitionCompleted(TransitionEventArgs eventArgs)
        {
            var handler = TransitionCompleted;
            if (handler != null)
            {
                handler(this, eventArgs);
            }
        }

        /// <summary>
        /// Creates a new <see cref="T:StateMachine.StateMachine`2"/> using specified context.
        /// </summary>
        /// <param name="context">Arbitrary context, which will be passed to the states in <see cref="IState{TContext}.OnEnter"/> and <see cref="IState{TContext}.OnExit"/>.</param>
        /// <param name="factoryFunc">State factory function. Determines whether the state is created on each transition or whether it's a single instance. By default, uses reflection to create states using parameterless constructor.</param>
        private StateMachine(TContext context, Func<Type, IState<TContext>> factoryFunc)
        {
            this.factoryFunc = factoryFunc ?? (tpe => (IState<TContext>) Activator.CreateInstance(tpe));
            Context = context;
        }

        /// <summary>
        /// Raised when the transition between states has been completed
        /// </summary>
        public event EventHandler<TransitionEventArgs> TransitionCompleted;

        private void ExecuteTrigger(TTrigger trigger)
        {
            Trigger<TContext, TTrigger> matchingTrigger;
            if (!currentState.Triggers.TryGetValue(trigger, out matchingTrigger))
            {
                if (onInvalidTrigger == null)
                {
                    throw new ExecutionException(string.Format("Trigger {0} is not valid for state {1}", trigger,
                        currentState.StateType.Name));
                }

                onInvalidTrigger(currentState.StateType, trigger);
                return;
            }

            if (matchingTrigger.Guard == null)
            {
                Transition(matchingTrigger.Target);
                return;
            }

            if (!matchingTrigger.Guard(Context))
            {
                return;
            }
            Transition(matchingTrigger.Target);
            //EnterNewState(ExitCurrentState(), matchingTrigger.Target);
        }

        /// <summary>
        /// Resumes the state machine from the specified state
        /// </summary>
        /// <typeparam name="TState">Type of the state</typeparam>
        /// <exception cref="StateNotFoundException">Thrown when specified state to resume from is not found</exception>
        public void ResumeFrom<TState>() where TState : IState<TContext>
        {
            ResumeFrom(typeof(TState));
        }

        /// <summary>
        /// Resumes the state machine from the specified state. Exposed mostly for reflection-friendly use.
        /// </summary>
        /// <param name="stateType">Type of the state to resume from</param>
        /// <exception cref="ExecutionException">Thrown when the type passed does not imlpement the <see cref="T:StateMachine.IState`1"/> interface</exception>
        /// <exception cref="StateNotFoundException">Thrown when the state is not found in the state machine</exception>
        public void ResumeFrom(Type stateType)
        {
            if (!typeof (IState<TContext>).GetTypeInfo().IsAssignableFrom(stateType.GetTypeInfo()))
            {
                throw new ExecutionException(string.Format("Type passed ({0}) is not assignable from IState<{1}>",
                    stateType, typeof (TContext)));
            }

            State<TContext, TTrigger> resumeState;
            if (states.TryGetValue(stateType, out resumeState))
            {
                Transition(resumeState, resume: true);
            }
            else
            {
                throw new StateNotFoundException(
                    string.Format("Cannot resume from state {0}; given state wasn't found in the state machine",
                        stateType));
            }
        }

        /// <summary>
        /// Triggers the transition
        /// </summary>
        /// <param name="trigger">Transition to be triggered from current state</param>
        /// <exception cref="ExecutionException">Thrown when the <see cref="Builder.OnInvalidTrigger"/> was not specified and passed <paramref name="trigger"/> is not valid for current state</exception>
        public void Trigger(TTrigger trigger)
        {
            lock (_lock)
            {
                ExecuteTrigger(trigger);
            }
        }

        /// <summary>
        /// Gets the context, which is also passed to states on transitions
        /// </summary>
        public TContext Context { get; private set; }
    }
}