﻿namespace StateMachine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Core;
    using Exceptions;

    public sealed partial class StateMachine<TContext, TTrigger>
    {
        /// <summary>
        /// A builder class which allows creation of the <see cref="T:StateMachine.StateMachine`2"/>
        /// </summary>
        public class Builder
        {
            private readonly IDictionary<Type, StateConfiguration> stateConfigurations;
            private StateConfiguration initialState;

            private readonly TContext context;
            private readonly Func<Type, IState<TContext>> factoryFunc;
            private bool isFactoryRequired;

            /// <summary>
            /// Gets or sets the action which will be executed when the invalid trigger in a state is raised.
            /// </summary>
            public Action<Type, TTrigger> OnInvalidTrigger { get; set; }

            /// <summary>
            /// Creates a new instance of <see cref="Builder"/>
            /// </summary>
            public Builder()
            {
                stateConfigurations = new Dictionary<Type, StateConfiguration>();
            }

            /// <summary>
            /// Creates a new instance of <see cref="Builder"/> using specified context.
            /// </summary>
            /// <param name="context">Context which will be passed to states on entry and exit</param>
            public Builder(TContext context)
            {
                stateConfigurations = new Dictionary<Type, StateConfiguration>();
                this.context = context;
            }

            /// <summary>
            /// Creates a new instance of <see cref="Builder"/> using specified context and state factory.
            /// </summary>
            /// <param name="context">Context which will be passed to states on entry and exit</param>
            /// <param name="factoryFunc">Factory function for states</param>
            public Builder(TContext context, Func<Type, IState<TContext>> factoryFunc)
            {
                this.context = context;
                this.factoryFunc = factoryFunc;
                stateConfigurations = new Dictionary<Type, StateConfiguration>();
            }

            /// <summary>
            /// Configures a state
            /// </summary>
            /// <typeparam name="TState">State type</typeparam>
            /// <returns>A <see cref="StateConfiguration"/> instance, allowing to set up trigger actions</returns>
            public StateConfiguration State<TState>() where TState : IState<TContext>
            {
                var type = typeof (TState);

                isFactoryRequired = isFactoryRequired || NoParameterlessConstructor(type);

                StateConfiguration configuration;
                if (stateConfigurations.TryGetValue(typeof (TState), out configuration))
                {
                    return configuration;
                }

                configuration = new StateConfiguration(type, this);
                stateConfigurations.Add(type, configuration);

                return configuration;
            }

            private bool NoParameterlessConstructor(Type type)
            {
                return type.GetTypeInfo().DeclaredConstructors.All(t => t.GetParameters().Length != 0);
            }

            private void SetInitialState(StateConfiguration initialStateConfiguration)
            {
                initialState = initialStateConfiguration;
            }

            /// <summary>
            /// Builds a new instance of <see cref="T:StateMachine.StateMachine`2"/> using specified states and settings.
            /// </summary>
            /// <exception cref="InitializationException">Thrown when the state factory (<see cref="T:StateMachine.IStateFactory`1"/>) is required, but not specified.</exception>
            /// <exception cref="StateNotFoundException">Thrown when the initial state is not specified</exception>
            /// <returns>A new instance of <see cref="T:StateMachine.StateMachine`2"/></returns>
            public StateMachine<TContext, TTrigger> Build()
            {
                if (isFactoryRequired && factoryFunc == null)
                    throw new InitializationException("State factory function is required but was not supplied");

                if (initialState == null)
                    throw new StateNotFoundException("Initial state was not specified");

                var states = stateConfigurations.ToDictionary(x => x.Key,
                    x => new State<TContext, TTrigger>(x.Value.StateType));

                foreach (var state in states)
                {
                    var triggers =
                        stateConfigurations[state.Key].Triggers.Select(x => new Trigger<TContext, TTrigger>(x.Trigger)
                        {
                            Guard = x.GuardClause,
                            Target = x.TargetState != null ? states[x.TargetState.StateType] : null,
                        });
                    state.Value.Triggers = triggers.ToDictionary(x => x.Value, x => x);
                }

                var stateMachine = new StateMachine<TContext, TTrigger>(context, factoryFunc)
                {
                    onInvalidTrigger = OnInvalidTrigger,
                    states = states
                };

                var initial = states[initialState.StateType];

                stateMachine.Transition(initial);

                return stateMachine;
            }

            /// <summary>
            /// Allows configuration of triggers
            /// </summary>
            public class TriggerConfiguration
            {
                private readonly StateConfiguration owningStateConfiguration;
                private readonly TTrigger trigger;
                private StateConfiguration targetState;
                private readonly Func<TContext, bool> guardClause;

                internal TTrigger Trigger
                {
                    get { return trigger; }
                }

                internal StateConfiguration TargetState
                {
                    get { return targetState; }
                }

                internal Func<TContext, bool> GuardClause
                {
                    get { return guardClause; }
                }

                /// <summary>
                /// Creates a new <see cref="StateMachine.StateMachine{TContext,TTrigger}.Builder.TriggerConfiguration"/> using the specified trigger value and owning <see cref="StateConfiguration"/>
                /// </summary>
                /// <param name="trigger">Trigger value</param>
                /// <param name="owningStateConfiguration">Owning state configuration</param>
                internal TriggerConfiguration(TTrigger trigger, StateConfiguration owningStateConfiguration)
                {
                    this.trigger = trigger;
                    this.owningStateConfiguration = owningStateConfiguration;
                }

                /// <summary>
                /// Creates a new <see cref="StateMachine.StateMachine{TContext,TTrigger}.Builder.TriggerConfiguration"/> using the specified trigger value, guard clause and owning <see cref="StateConfiguration"/>
                /// </summary>
                /// <param name="trigger">Trigger value</param>
                /// <param name="guardClause">Guard clause for the trigger</param>
                /// <param name="owningStateConfiguration">Owning state configuration</param>
                internal TriggerConfiguration(TTrigger trigger, Func<TContext, bool> guardClause,
                    StateConfiguration owningStateConfiguration)
                {
                    this.guardClause = guardClause;
                    this.trigger = trigger;
                    this.owningStateConfiguration = owningStateConfiguration;
                }

                /// <summary>
                /// Specifies to which state this trigger will transition the enclosing <see cref="StateConfiguration"/>
                /// </summary>
                /// <typeparam name="TState">Target state type</typeparam>
                /// <returns>A new <see cref="StateConfiguration"/> for the target state</returns>
                public StateConfiguration GoesTo<TState>() where TState : IState<TContext>
                {
                    targetState = owningStateConfiguration.EnclosingBuilder.State<TState>();
                    return
                        owningStateConfiguration.EnclosingBuilder.stateConfigurations[owningStateConfiguration.StateType
                            ];
                }
            }

            /// <summary>
            /// Allows configuration of states
            /// </summary>
            public class StateConfiguration
            {
                private readonly Builder enclosingBuilder;
                private readonly Type stateType;
                private readonly List<TriggerConfiguration> triggers;

                internal Builder EnclosingBuilder
                {
                    get { return enclosingBuilder; }
                }

                internal IEnumerable<TriggerConfiguration> Triggers
                {
                    get { return triggers; }
                }

                internal Type StateType
                {
                    get { return stateType; }
                }

                /// <summary>
                /// Creates a new configuration for state
                /// </summary>
                /// <param name="stateType">Type of the state</param>
                /// <param name="enclosingBuilder">Owning builder</param>
                internal StateConfiguration(Type stateType, Builder enclosingBuilder)
                {
                    this.stateType = stateType;
                    this.enclosingBuilder = enclosingBuilder;
                    triggers = new List<TriggerConfiguration>();
                }

                /// <summary>
                /// Sets the state as initial state of the state machine. If there are multiple states marked as initial, last one wins.
                /// </summary>
                /// <returns>Current <see cref="StateMachine.StateMachine{TContext,TTrigger}.Builder.StateConfiguration"/></returns>
                public StateConfiguration IsInitialState()
                {
                    enclosingBuilder.SetInitialState(this);
                    return this;
                }

                /// <summary>
                /// Adds a trigger to a state
                /// </summary>
                /// <param name="trigger">Transition trigger</param>
                /// <exception cref="InitializationException">Thrown either when there's at least one guarded transition present for current state or if the trigger has already been configured for the state</exception>
                /// <returns>New <see cref="TriggerConfiguration"/> for current <see cref="StateMachine.StateMachine{TContext,TTrigger}.Builder.StateConfiguration"/></returns>
                public TriggerConfiguration On(TTrigger trigger)
                {
                    var existingConfig = triggers.FirstOrDefault(x => x.Trigger.Equals(trigger));
                    if (existingConfig != null)
                    {
                        if (existingConfig.GuardClause != null)
                        {
                            throw new InitializationException(
                                string.Format(
                                    "State {0} has at least one guarded transition configured on trigger {1} already. A state cannot have both guardless and guarded transitions at the same time",
                                    stateType.Name, trigger));
                        }
                        throw new InitializationException(
                            string.Format("Trigger {0} has already been configured for state {1}", trigger,
                                stateType.Name));
                    }

                    var newConfiguration = new TriggerConfiguration(trigger, this);
                    triggers.Add(newConfiguration);

                    return newConfiguration;
                }

                /// <summary>
                /// Adds a guarded trigger to a state
                /// </summary>
                /// <param name="trigger">Transition trigger</param>
                /// <param name="guardClause">Guard clause for the trigger</param>
                /// <exception cref="ArgumentNullException">Thrown when the <paramref name="guardClause"/> is <c>null</c></exception>
                /// <exception cref="InitializationException">Thrown when the state already has an unguarded transition for a trigger.</exception>
                /// <returns>A new <see cref="TriggerConfiguration"/> for current <see cref="StateMachine.StateMachine{TContext,TTrigger}.Builder.StateConfiguration"/></returns>
                public TriggerConfiguration On(TTrigger trigger, Func<TContext, bool> guardClause)
                {
                    if (guardClause == null)
                        throw new ArgumentNullException("guardClause");

                    var existingConfig = triggers.FirstOrDefault(x => x.Trigger.Equals(trigger));
                    if (existingConfig != null && existingConfig.GuardClause == null)
                    {
                        throw new InitializationException(
                            string.Format(
                                "State {0} has an unguarded transition for trigger {1}, you cannot add guarded transitions to this state as well.",
                                stateType.Name, trigger));
                    }

                    var newConfiguration = new TriggerConfiguration(trigger, guardClause, this);
                    triggers.Add(newConfiguration);

                    return newConfiguration;
                }
            }
        }
    }
}
