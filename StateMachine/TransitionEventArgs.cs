namespace StateMachine
{
    using System;

    /// <summary>
    /// Raised in the event when the transition from state to state is complete
    /// </summary>
    public class TransitionEventArgs : EventArgs
    {
        private readonly Type source;
        private readonly Type target;

        /// <summary>
        /// Source state type
        /// </summary>
        public Type Source { get { return source; } }

        /// <summary>
        /// Target state type
        /// </summary>
        public Type Target { get { return target; } }

        /// <summary>
        /// Creates new instance of <see cref="TransitionEventArgs"/>
        /// </summary>
        /// <param name="source">Source state type</param>
        /// <param name="target">Target state type</param>
        public TransitionEventArgs(Type source, Type target)
        {
            this.source = source;
            this.target = target;
        }
    }
}