﻿namespace StateMachine
{
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for a state with asynchronous transitions in the <see cref="T:StateMachine.AsyncStateMachine`2"/>
    /// <typeparam name="TContext">Context type</typeparam>
    /// </summary>
    public interface IAsyncState<in TContext>
    {
        /// <summary>
        /// Called by the state machine when the state is entered to.
        /// </summary>
        /// <param name="context">Arbitrary context object passed from the state machine.</param>
        Task OnEnterAsync(TContext context);

        /// <summary>
        /// called by the state machine when the state is exited from.
        /// </summary>
        /// <param name="context">Arbitrary context object passed from the state machine.</param>
        Task OnExitAsync(TContext context);
    }
}