namespace StateMachine
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using Core;
    using Exceptions;

    /// <summary>
    /// A finite state machine with async transitions
    /// </summary>
    /// <typeparam name="TTrigger">Trigger type</typeparam>
    /// <typeparam name="TContext">Context type</typeparam>
    public sealed partial class AsyncStateMachine<TContext, TTrigger>
    {
        private readonly Func<Type, IAsyncState<TContext>> factoryFunc;
        private IReadOnlyDictionary<Type, AsyncState<TContext, TTrigger>> states;
        private AsyncState<TContext, TTrigger> currentState;
        private Action<Type, TTrigger> onInvalidTrigger;
        private readonly SemaphoreSlim semaphore = new SemaphoreSlim(1);

        /// <summary>
        /// Gets the context, which is also passed to states on transitions
        /// </summary>
        public TContext Context { get; private set; }

        private IAsyncState<TContext> CreateInstance(Type type)
        {
            var instance = factoryFunc(type);

            if (instance == null)
            {
                throw new StateMachineException("Null is not a valid value for instance of state type " + type);
            }

            return instance;
        }

        /// <summary>
        /// Transitions from current state to another state asynchronously
        /// </summary>
        /// <param name="newState">Target state the machine should transition to</param>
        /// <param name="resume">Indicates whether the machine should 'resume' from the target state or perform a regular transition</param>
        private async Task TransitionAsync(AsyncState<TContext, TTrigger> newState, bool resume = false)
        {
            if (currentState != null && !resume)
            {
                await currentState.Instance.OnExitAsync(Context).ConfigureAwait(false);
            }

            var previousState = currentState;

            if (newState != null)
            {
                newState.Instance = CreateInstance(newState.StateType);

                if (!resume)
                {
                    await newState.Instance.OnEnterAsync(Context).ConfigureAwait(false);
                }

                currentState = newState;
                OnTransitionCompleted(new TransitionEventArgs(previousState != null ? previousState.StateType : null,
                    currentState.StateType));
            }
            else
            {
                currentState = null;
            }
        }

        /// <summary>
        /// Creates a new <see cref="T:StateMachine.AsyncStateMachine`2"/> using specified context.
        /// </summary>
        /// <param name="context">Arbitrary context, which will be passed to the states in <see cref="IAsyncState{TContext}.OnEnterAsync"/> and <see cref="IAsyncState{TContext}.OnExitAsync"/>.</param>
        /// <param name="factoryFunc">State factory function. Determines whether the state is created on each transition or whether it's a single instance. By default, uses reflection to create states using parameterless constructor.</param>
        private AsyncStateMachine(TContext context, Func<Type, IAsyncState<TContext>> factoryFunc)
        {
            this.factoryFunc = factoryFunc ?? (tpe => (IAsyncState<TContext>) Activator.CreateInstance(tpe));
            Context = context;
        }

        /// <summary>
        /// Resumes the state machine from the specified state asynchronously.
        /// </summary>
        /// <typeparam name="TState">Type of the state to resume from</typeparam>
        /// <exception cref="StateNotFoundException">Thrown when specified state to resume from is not found</exception>
        public Task ResumeFromAsync<TState>() where TState : IAsyncState<TContext>
        {
            return ResumeFromAsync(typeof(TState));
        }

        /// <summary>
        /// Resumes the state machine from the specified state. Exposed mostly for reflection-friendly use.
        /// </summary>
        /// <param name="stateType">Type of the state to resume from</param>
        /// <exception cref="ExecutionException">Thrown when the type passed does not imlpement the <see cref="T:StateMachine.IAsyncState`1"/> interface</exception>
        /// <exception cref="StateNotFoundException">Thrown when the state is not found in the state machine</exception>
        public async Task ResumeFromAsync(Type stateType)
        {
            if (!typeof (IAsyncState<TContext>).GetTypeInfo().IsAssignableFrom(stateType.GetTypeInfo()))
            {
                throw new ExecutionException(string.Format("Type passed ({0}) is not assignable from IAsyncState<{1}>",
                    stateType, typeof (TContext)));
            }

            AsyncState<TContext, TTrigger> resumeState;
            if (states.TryGetValue(stateType, out resumeState))
            {
                await TransitionAsync(resumeState, resume: true).ConfigureAwait(false);
            }
            else
            {
                throw new StateNotFoundException(
                    string.Format("Cannot resume from state {0}; given state wasn't found in the state machine",
                        stateType));
            }
        }

        private async Task ExecuteTriggerAsync(TTrigger trigger)
        {
            AsyncTrigger<TContext, TTrigger> matchingTrigger;
            if (!currentState.Triggers.TryGetValue(trigger, out matchingTrigger))
            {
                if (onInvalidTrigger == null)
                {
                    throw new ExecutionException(string.Format("Trigger {0} is not valid for state {1}", trigger,
                        currentState.StateType.Name));
                }

                onInvalidTrigger(currentState.StateType, trigger);
                return;
            }

            if (matchingTrigger.Guard == null)
            {
                await TransitionAsync(matchingTrigger.Target).ConfigureAwait(false);
                return;
            }

            if (!matchingTrigger.Guard(Context))
            {
                return;
            }

            await TransitionAsync(matchingTrigger.Target).ConfigureAwait(false);
        }

        /// <summary>
        /// Triggers the transition asynchronously
        /// </summary>
        /// <param name="trigger">Transition to be triggered from current state</param>
        /// <exception cref="ExecutionException">Thrown when the <see cref="Builder.OnInvalidTrigger"/> was not specified and passed <paramref name="trigger"/> is not valid for current state</exception>
        public async Task TriggerAsync(TTrigger trigger)
        {
            await semaphore.WaitAsync().ConfigureAwait(false);
            try
            {
                await ExecuteTriggerAsync(trigger).ConfigureAwait(false);
            }
            finally
            {
                semaphore.Release();
            }
        }

        /// <summary>
        /// Raised when the transition between states has been completed
        /// </summary>
        public event EventHandler<TransitionEventArgs> TransitionCompleted;

        private void OnTransitionCompleted(TransitionEventArgs eventArgs)
        {
            var handler = TransitionCompleted;
            if (handler != null)
            {
                handler(this, eventArgs);
            }
        }
    }
}