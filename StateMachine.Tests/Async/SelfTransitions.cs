﻿namespace StateMachine.Tests.Async
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;

    public class SelfTransitionsTest
    {
        public class BaseState : IAsyncState<string>
        {
            public Task OnEnterAsync(string context)
            {
                return Task.FromResult(false);
            }

            public Task OnExitAsync(string context)
            {
                return Task.FromResult(false);
            }
        }

        public class Start : BaseState
        {
        }

        public class Middle : BaseState
        {
        }

        public class End : BaseState
        {
        }


        [Fact(DisplayName = "Degenerate, looping state machine without end state is allowed.")]
        public async Task DegenerateStateMachine()
        {
            var builder = new AsyncStateMachine<string, Trigger>.Builder();

            builder.State<Start>()
                .IsInitialState()
                .On(Trigger.Loop)
                .GoesTo<Start>();

            var stateMachine = await builder.BuildAsync();
            int transitionCounter = 0;
            stateMachine.TransitionCompleted += (sender, args) => transitionCounter++;
            await stateMachine.TriggerAsync(Trigger.Loop);
            await stateMachine.TriggerAsync(Trigger.Loop);
            Assert.Equal(2, transitionCounter);
        }

        [Fact(DisplayName = "Multi-state transition loop is allowed.")]
        public async Task StateLoop()
        {
            var list = new List<KeyValuePair<Type, Type>>();

            var builder = new AsyncStateMachine<string, Trigger>.Builder();

            builder.State<Start>()
                .IsInitialState()
                .On(Trigger.Loop)
                .GoesTo<Start>()
                .On(Trigger.MoveNext)
                .GoesTo<Middle>();

            builder.State<Middle>()
                .On(Trigger.Loop)
                .GoesTo<Middle>()
                .On(Trigger.MoveNext)
                .GoesTo<End>();

            var stateMachine = await builder.BuildAsync();
            stateMachine.TransitionCompleted +=
                (sender, args) => list.Add(new KeyValuePair<Type, Type>(args.Source, args.Target));
            await stateMachine.TriggerAsync(Trigger.Loop);
            await stateMachine.TriggerAsync(Trigger.MoveNext);
            await stateMachine.TriggerAsync(Trigger.Loop);
            await stateMachine.TriggerAsync(Trigger.MoveNext);

            Assert.Equal(4, list.Count);
            Assert.True(list.SequenceEqual(new[]
            {
                new KeyValuePair<Type, Type>(typeof (Start), typeof (Start)),
                new KeyValuePair<Type, Type>(typeof (Start), typeof (Middle)),
                new KeyValuePair<Type, Type>(typeof (Middle), typeof (Middle)),
                new KeyValuePair<Type, Type>(typeof (Middle), typeof (End))
            }));
        }
    }
}
