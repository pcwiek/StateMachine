﻿namespace StateMachine.Tests.Async
{
    using System.Threading.Tasks;

    public class BasicState : IAsyncState<object>
    {
        public Task OnEnterAsync(object context)
        {
            return Task.FromResult(false);
        }

        public Task OnExitAsync(object context)
        {
            return Task.FromResult(false);
        }
    }

    public class Start : BasicState { }

    public class Middle : BasicState { }

    public class End : BasicState { }
}
