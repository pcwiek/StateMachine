﻿namespace StateMachine.Tests.Async
{
    using System.Threading.Tasks;
    using Xunit;

    public class Guards
    {
        public class Counter
        {
            public int Count { get; set; }
        }

        public class Start : IAsyncState<Counter>
        {
            public Task OnEnterAsync(Counter context)
            {
                context.Count++;
                return Task.FromResult(false);
            }

            public Task OnExitAsync(Counter context)
            {
                return Task.FromResult(false);
            }
        }

        public class End : IAsyncState<Counter>
        {
            public Task OnEnterAsync(Counter context)
            {
                return Task.FromResult(false);
            }

            public Task OnExitAsync(Counter context)
            {
                return Task.FromResult(false);
            }
        }

        [Fact(DisplayName = "If the guard evaluates to 'false', transition should not be triggered")]
        public async Task TransitionIsNotTriggeredIfGuardEvaluatesToFalse()
        {
            var ctx = new Counter();
            int localCount = 0;
            var builder = new AsyncStateMachine<Counter, Trigger>.Builder(ctx);
            builder.State<Start>()
                .IsInitialState()
                .On(Trigger.MoveNext, counter => counter.Count > 4)
                .GoesTo<End>()
                .On(Trigger.Loop)
                .GoesTo<Start>();

            var machine = await builder.BuildAsync();
            machine.TransitionCompleted += (sender, args) => localCount++;
            await machine.TriggerAsync(Trigger.MoveNext);

            Assert.Equal(0, localCount);
        }

        [Fact(DisplayName = "If the guard evaluates to 'true', transition should be triggered")]
        public async Task TransitionIsTriggeredIfGuardEvaluatesToTrue()
        {
            var ctx = new Counter();
            int localCount = 0;
            var builder = new AsyncStateMachine<Counter, Trigger>.Builder(ctx);
            builder.State<Start>()
                .IsInitialState()
                .On(Trigger.MoveNext, counter => counter.Count > 4)
                .GoesTo<End>()
                .On(Trigger.Loop)
                .GoesTo<Start>();

            var machine = await builder.BuildAsync();
            machine.TransitionCompleted += (sender, args) => localCount++;
            await machine.TriggerAsync(Trigger.MoveNext);

            Assert.Equal(0, localCount);

            ctx.Count = 10;
            await machine.TriggerAsync(Trigger.MoveNext);

            Assert.Equal(1, localCount);
        }
    }
}
