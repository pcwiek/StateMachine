﻿namespace StateMachine.Tests.Async
{
    using System;
    using System.Threading.Tasks;
    using Xunit;

    public class ExceptionsDuringTransitions
    {
        public class CountingContext
        {
            public int Count { get; set; }
        }

        public abstract class BaseState : IAsyncState<CountingContext>
        {
            public virtual Task OnEnterAsync(CountingContext context)
            {
                return Task.FromResult(false);
            }

            public virtual Task OnExitAsync(CountingContext context)
            {
                return Task.FromResult(false);
            }
        }

        public class RegularState : BaseState
        {
            public override Task OnExitAsync(CountingContext context)
            {
                context.Count++;
                return base.OnExitAsync(context);
            }
        }

        public class ThrowingState : BaseState
        {
            public override Task OnEnterAsync(CountingContext context)
            {
                throw new InvalidOperationException("Nope!");
            }
        }

        public class ExitThrowingState : BaseState
        {
            public override Task OnExitAsync(CountingContext context)
            {
                throw new InvalidOperationException("Nope!");
            }
        }


        [Fact(DisplayName = "Exception in 'OnEnter' hook will abort the state transition and move to previous state (if available)")]
        public async Task ExceptionInEnterHookWillAbortTransition()
        {
            var ctx = new CountingContext();
            var builder = new AsyncStateMachine<CountingContext, Trigger>.Builder(ctx);

            builder.State<RegularState>().IsInitialState().On(Trigger.MoveNext).GoesTo<ThrowingState>();

            var machine = await builder.BuildAsync();
            await Assert.ThrowsAsync<InvalidOperationException>(() => machine.TriggerAsync(Trigger.MoveNext));
            Assert.Equal(1, ctx.Count);

            // State machine should be back in RegularState now. MoveNext should be available, 'OnExit' will fire again.
            await Assert.ThrowsAsync<InvalidOperationException>(() => machine.TriggerAsync(Trigger.MoveNext));
            Assert.Equal(2, ctx.Count);
        }

        [Fact(DisplayName = "Exception in 'OnEnter' hook will abort the state transition and stay in current state")]
        public async Task ExceptionInExitHookWillAbortTransition()
        {
            var ctx = new CountingContext();
            var builder = new AsyncStateMachine<CountingContext, Trigger>.Builder(ctx);

            builder.State<ExitThrowingState>().IsInitialState().On(Trigger.MoveNext).GoesTo<RegularState>();

            var machine = await builder.BuildAsync();
            await Assert.ThrowsAsync<InvalidOperationException>(() => machine.TriggerAsync(Trigger.MoveNext));

            // State machine should be back in ExitThrowingState now. MoveNext should be available, 'OnExit' will fire (and crash) again.
            await Assert.ThrowsAsync<InvalidOperationException>(() => machine.TriggerAsync(Trigger.MoveNext));
        }

        [Fact(DisplayName = "If the initial state throws in OnEnter hook, it will abort state machine creation")]
        public async Task ThrowOnInitialEntryWillAbortMachineCreation()
        {
            var ctx = new CountingContext();
            var builder = new AsyncStateMachine<CountingContext, Trigger>.Builder(ctx);

            builder.State<ThrowingState>().IsInitialState().On(Trigger.MoveNext).GoesTo<RegularState>();

            await Assert.ThrowsAsync<InvalidOperationException>(() => builder.BuildAsync());
        }
    }
}
