﻿namespace StateMachine.Tests.Async
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Exceptions;
    using Xunit;

    public class ComplexState : IAsyncState<object>
    {
        public ComplexState(string str)
        {
        }

        public Task OnEnterAsync(object context)
        {
            return Task.FromResult(false);
        }

        public Task OnExitAsync(object context)
        {
            return Task.FromResult(false);
        }
    }

    public class BuildingTests
    {
        private readonly Func<Type, IAsyncState<object>> factoryFunc;

        public BuildingTests()
        {
            factoryFunc = type =>
            {
                if (type == typeof(ComplexState))
                    return new ComplexState("Test");
                if (type == typeof(Start))
                    return new Start();

                return null;
            };
        }

        [Fact]
        public async Task FactoryIsNeededWhenUsingStatesWithNoParameterlessConstructor()
        {
            var builder = new AsyncStateMachine<object, Trigger>.Builder();
            builder.State<ComplexState>()
                   .IsInitialState();

            await Assert.ThrowsAsync<InitializationException>(() => builder.BuildAsync());
        }

        [Fact]
        public async Task StateFactoryUsedForComplexType()
        {
            var builder = new AsyncStateMachine<object, Trigger>.Builder(null, factoryFunc);

            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<ComplexState>();

            var sm = await builder.BuildAsync();
            var list = new List<KeyValuePair<Type, Type>>();
            sm.TransitionCompleted += (sender, args) => list.Add(new KeyValuePair<Type, Type>(args.Source, args.Target));
            await sm.TriggerAsync(Trigger.MoveNext);
            Assert.Equal(1, list.Count);
            Assert.Equal(typeof(Start), list.First().Key);
            Assert.Equal(typeof(ComplexState), list.First().Value);
        }

        [Fact(DisplayName = "Factory function cannot return null")]
        public async Task FactoryHasToResolveAllStates()
        {
            var builder = new AsyncStateMachine<object, Trigger>.Builder(null, factoryFunc);

            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<ComplexState>();

            builder.State<ComplexState>()
                   .On(Trigger.MoveNext)
                   .GoesTo<End>();

            var sm = await builder.BuildAsync();

            await sm.TriggerAsync(Trigger.MoveNext);
            await Assert.ThrowsAsync<StateMachineException>(() => sm.TriggerAsync(Trigger.MoveNext));
        }

        [Fact(DisplayName = "Invalid trigger handler will be called (if it's specified) on invalid trigger.")]
        public async Task InvalidTriggerHandlerIsCalledIfSpecified()
        {
            string error = null;

            var builder = new AsyncStateMachine<object, Trigger>.Builder()
            {
                OnInvalidTrigger =
                    (type, trigger) =>
                        error = string.Format("Type {0} does not have trigger {1} specified.", type, trigger)
            };
            builder.State<Start>()
                .IsInitialState()
                .On(Trigger.MoveNext)
                .GoesTo<End>();

            var machine = await builder.BuildAsync();
            await machine.TriggerAsync(Trigger.MoveNext);

            Assert.Null(error);

            await machine.TriggerAsync(Trigger.MoveNext);

            Assert.Equal("Type StateMachine.Tests.Async.End does not have trigger MoveNext specified.", error);
        }

        [Fact]
        public async Task InitialStateHasToBeSpecified()
        {
            var builder = new AsyncStateMachine<object, Trigger>.Builder();
            builder.State<Start>()
                   .On(Trigger.MoveNext)
                   .GoesTo<End>();

            await Assert.ThrowsAsync<StateNotFoundException>(() => builder.BuildAsync());
        }
    }
}
