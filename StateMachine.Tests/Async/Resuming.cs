﻿namespace StateMachine.Tests.Async
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Exceptions;
    using Xunit;

    public class ResumedState : IAsyncState<IList<int>>
    {
        public Task OnEnterAsync(IList<int> context)
        {
            context.Add(1);
            return Task.FromResult(false);
        }

        public Task OnExitAsync(IList<int> context)
        {
            return Task.FromResult(false);
        }
    }

    public class WrongContextState : IAsyncState<string>
    {
        public Task OnEnterAsync(string context)
        {
            return Task.FromResult(false);
        }

        public Task OnExitAsync(string context)
        {
            return Task.FromResult(false);
        }
    }

    public class Resuming
    {
        [Fact]
        public async Task CanResumeFromValidState()
        {
            var builder = new AsyncStateMachine<IList<int>, Trigger>.Builder();
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<Middle>();

            builder.State<Middle>()
                   .On(Trigger.Loop)
                   .GoesTo<Middle>();

            var sm = await builder.BuildAsync();

            await sm.ResumeFromAsync<Middle>();

            // Should not throw
            await sm.TriggerAsync(Trigger.Loop);
        }

        [Fact]
        public async Task CannotResumeFromStateNotPresentInTheMachine()
        {
            var builder = new AsyncStateMachine<object, Trigger>.Builder();
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.Loop)
                   .GoesTo<Start>();

            var sm = await builder.BuildAsync();

            await Assert.ThrowsAsync<StateNotFoundException>(() => sm.ResumeFromAsync<Middle>());
        }

        [Fact]
        public async Task CanResumeFromValidStateNonGeneric()
        {
            var builder = new AsyncStateMachine<object, Trigger>.Builder();
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<Middle>();

            builder.State<Middle>()
                   .On(Trigger.Loop)
                   .GoesTo<Middle>();

            var sm = await builder.BuildAsync();

            await sm.ResumeFromAsync(typeof(Middle));
            // Should not throw
            await sm.TriggerAsync(Trigger.Loop);
        }

        [Fact]
        public async Task CannotResumeFromStateNotPresentInTheMachineNonGeneric()
        {
            var builder = new AsyncStateMachine<object, Trigger>.Builder();
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.Loop)
                   .GoesTo<Start>();

            var sm = await builder.BuildAsync();

            await Assert.ThrowsAsync<StateNotFoundException>(() => sm.ResumeFromAsync(typeof(Middle)));
        }

        [Fact]
        public async Task CannotResumeFromTypeNotImplementingState()
        {
            var builder = new AsyncStateMachine<object, Trigger>.Builder();
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.Loop)
                   .GoesTo<Start>();

            var sm = await builder.BuildAsync();

            await Assert.ThrowsAsync<ExecutionException>(() => sm.ResumeFromAsync(typeof(string)));
        }

        [Fact]
        public async Task ResumedStateDoesNotCallOnEnter()
        {
            var enterList = new List<int>();
            var builder = new AsyncStateMachine<IList<int>, Trigger>.Builder(enterList);
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<ResumedState>();

            builder.State<ResumedState>()
                   .On(Trigger.Loop)
                   .GoesTo<ResumedState>();

            var sm = await builder.BuildAsync();

            await sm.ResumeFromAsync(typeof(ResumedState));
            Assert.Equal(0, enterList.Sum());
        }

        [Fact]
        public async Task CannotResumeFromStateNotImplementingCorrectContextInterfaceNonGeneric()
        {
            var enterList = new List<int>();
            var builder = new AsyncStateMachine<IList<int>, Trigger>.Builder(enterList);

            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<ResumedState>();

            builder.State<ResumedState>()
                   .On(Trigger.Loop)
                   .GoesTo<ResumedState>();

            var sm = await builder.BuildAsync();

            await Assert.ThrowsAsync<ExecutionException>(() => sm.ResumeFromAsync(typeof(WrongContextState)));
        }

        [Fact]
        public async Task OnEnterIsCalledIfTheStateIsNotResumed()
        {
            var enterList = new List<int>();
            var builder = new AsyncStateMachine<IList<int>, Trigger>.Builder(enterList);
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<ResumedState>();

            builder.State<ResumedState>()
                   .On(Trigger.Loop)
                   .GoesTo<ResumedState>();

            var sm = await builder.BuildAsync();

            await sm.TriggerAsync(Trigger.MoveNext);

            Assert.Equal(1, enterList.Sum());
        }
    }
}
