﻿namespace StateMachine.Tests.Async
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;

    public class ContextTests
    {
        public interface IIdentity<out T>
        {
            T Id { get; }
        }

        public class Start : IAsyncState<IIdentity<string>>
        {
            public Task OnEnterAsync(IIdentity<string> context)
            {
                return Task.FromResult(false);
            }

            public Task OnExitAsync(IIdentity<string> context)
            {
                return Task.FromResult(false);
            }
        }

        public class End : IAsyncState<IIdentity<object>>
        {
           public Task OnEnterAsync(IIdentity<object> context)
            {
                return Task.FromResult(false);                
            }

            public Task OnExitAsync(IIdentity<object> context)
            {
                return Task.FromResult(false);
            }
        }

        public class SimpleStart : IAsyncState<IList<string>>
        {
            public Task OnEnterAsync(IList<string> context)
            {
                context.Add("Enter: " + typeof(SimpleStart).Name);
                return Task.FromResult(false);
            }

            public Task OnExitAsync(IList<string> context)
            {
                context.Add("Exit: " + typeof(SimpleStart).Name);
                return Task.FromResult(false);
            }
        }

        public class SimpleEnd : IAsyncState<IList<string>>
        {
            public Task OnEnterAsync(IList<string> context)
            {
                context.Add("Enter: " + typeof(SimpleEnd).Name);
                return Task.FromResult(false);
            }

            public Task OnExitAsync(IList<string> context)
            {
                context.Add("Exit: " + typeof(SimpleEnd).Name);
                return Task.FromResult(false);
            }
        }

        [Fact(DisplayName = "Custom context instance can be passed to state machine")]
        public async Task PassingCustomContext()
        {
            var list = new List<string>();
            var builder = new AsyncStateMachine<IList<string>, Trigger>.Builder(list);

            builder.State<SimpleStart>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<SimpleEnd>();

            builder.State<SimpleEnd>()
                   .On(Trigger.Loop)
                   .GoesTo<SimpleEnd>();

            var sm = await builder.BuildAsync();
            await sm.TriggerAsync(Trigger.MoveNext);
            await sm.TriggerAsync(Trigger.Loop);

            // There's no last 'exit' from the last state, because it's... Last state. So, as name might suggest, there's no real exit from it.
            Assert.True(list.SequenceEqual(new[]
				                                 {
					                                 "Enter: SimpleStart",
													 "Exit: SimpleStart",
													 "Enter: SimpleEnd",
													 "Exit: SimpleEnd",
													 "Enter: SimpleEnd"
				                                 }));
        }

        [Fact(DisplayName = "If no context is specified, it will have the default value for the type")]
        public async Task ContextIsImplicitlyNull()
        {
            var builder = new AsyncStateMachine<IList<string>, Trigger>.Builder();

            builder.State<SimpleStart>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<SimpleEnd>();

            // Context is used on first transition, should fail here.
            await Assert.ThrowsAsync<NullReferenceException>(() => builder.BuildAsync());
        }

        [Fact(DisplayName = "States are covariant in context type parameter")]
        public async Task CovarianceTest()
        {
            var builder = new AsyncStateMachine<IIdentity<string>, Trigger>.Builder();
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<End>()
                   .On(Trigger.Loop)
                   .GoesTo<Start>();

            var sm = await builder.BuildAsync();
            var transitions = 0;
            sm.TransitionCompleted += (sender, args) => transitions++;
            await sm.TriggerAsync(Trigger.Loop);
            await sm.TriggerAsync(Trigger.MoveNext);
            Assert.Equal(2, transitions);
        }
    }
}
