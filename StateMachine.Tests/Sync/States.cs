﻿namespace StateMachine.Tests.Sync
{
    public class Start : IState<object>
    {
        public void OnEnter(object context)
        {
        }

        public void OnExit(object context)
        {
        }
    }

    public class Middle : IState<object>
    {
        public void OnEnter(object context)
        {
        }

        public void OnExit(object context)
        {
        }
    }

    public class End : IState<object>
    {
        public void OnEnter(object context)
        {
        }

        public void OnExit(object context)
        {
        }
    }
}
