﻿namespace StateMachine.Tests.Sync
{
    using System.Collections.Generic;
    using System.Linq;
    using Exceptions;
    using Xunit;

    public class ResumedState : IState<IList<int>>
    {
        /// <summary>
        /// Called by the state machine when the state is entered to.
        /// </summary>
        /// <param name="context">Arbitrary context object passed from the state machine.</param>
        public void OnEnter(IList<int> context)
        {
            context.Add(1);
        }

        /// <summary>
        /// called by the state machine when the state is exited from.
        /// </summary>
        /// <param name="context">Arbitrary context object passed from the state machine.</param>
        public void OnExit(IList<int> context)
        {
        }
    }

    public class WrongContextState : IState<string>
    {
        /// <summary>
        /// Called by the state machine when the state is entered to.
        /// </summary>
        /// <param name="context">Arbitrary context object passed from the state machine.</param>
        public void OnEnter(string context)
        {
        }

        /// <summary>
        /// called by the state machine when the state is exited from.
        /// </summary>
        /// <param name="context">Arbitrary context object passed from the state machine.</param>
        public void OnExit(string context)
        {
        }
    }

    public class Resuming
    {
        [Fact]
        public void CanResumeFromValidState()
        {
            var builder = new StateMachine<IList<int>, Trigger>.Builder();
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<Middle>();

            builder.State<Middle>()
                   .On(Trigger.Loop)
                   .GoesTo<Middle>();

            var sm = builder.Build();

            sm.ResumeFrom<Middle>();

            // Should not throw
            sm.Trigger(Trigger.Loop);
        }

        [Fact]
        public void CannotResumeFromStateNotPresentInTheMachine()
        {
            var builder = new StateMachine<object, Trigger>.Builder();
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.Loop)
                   .GoesTo<Start>();

            var sm = builder.Build();

            Assert.Throws<StateNotFoundException>(() => sm.ResumeFrom<Middle>());
        }

        [Fact]
        public void CanResumeFromValidStateNonGeneric()
        {
            var builder = new StateMachine<object, Trigger>.Builder();
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<Middle>();

            builder.State<Middle>()
                   .On(Trigger.Loop)
                   .GoesTo<Middle>();

            var sm = builder.Build();

            sm.ResumeFrom(typeof(Middle));
            // Should not throw
            sm.Trigger(Trigger.Loop);
        }

        [Fact]
        public void CannotResumeFromStateNotPresentInTheMachineNonGeneric()
        {
            var builder = new StateMachine<object, Trigger>.Builder();
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.Loop)
                   .GoesTo<Start>();

            var sm = builder.Build();

            Assert.Throws<StateNotFoundException>(() => sm.ResumeFrom(typeof(Middle)));
        }

        [Fact]
        public void CannotResumeFromTypeNotImplementingState()
        {
            var builder = new StateMachine<object, Trigger>.Builder();
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.Loop)
                   .GoesTo<Start>();

            var sm = builder.Build();

            Assert.Throws<ExecutionException>(() => sm.ResumeFrom(typeof(string)));
        }

        [Fact]
        public void ResumedStateDoesNotCallOnEnter()
        {
            var enterList = new List<int>();
            var builder = new StateMachine<IList<int>, Trigger>.Builder(enterList);
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<ResumedState>();

            builder.State<ResumedState>()
                   .On(Trigger.Loop)
                   .GoesTo<ResumedState>();

            var sm = builder.Build();

            sm.ResumeFrom(typeof(ResumedState));
            Assert.Equal(0, enterList.Sum());
        }

        [Fact]
        public void CannotResumeFromStateNotImplementingCorrectContextInterfaceNonGeneric()
        {
            var enterList = new List<int>();
            var builder = new StateMachine<IList<int>, Trigger>.Builder(enterList);

            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<ResumedState>();

            builder.State<ResumedState>()
                   .On(Trigger.Loop)
                   .GoesTo<ResumedState>();

            var sm = builder.Build();

            Assert.Throws<ExecutionException>(() => sm.ResumeFrom(typeof(WrongContextState)));
        }

        [Fact]
        public void OnEnterIsCalledIfTheStateIsNotResumed()
        {
            var enterList = new List<int>();
            var builder = new StateMachine<IList<int>, Trigger>.Builder(enterList);
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<ResumedState>();

            builder.State<ResumedState>()
                   .On(Trigger.Loop)
                   .GoesTo<ResumedState>();

            var sm = builder.Build();

            sm.Trigger(Trigger.MoveNext);

            Assert.Equal(1, enterList.Sum());
        }
    }
}
