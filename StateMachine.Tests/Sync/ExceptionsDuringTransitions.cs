﻿namespace StateMachine.Tests.Sync
{
    using System;
    using System.Threading.Tasks;
    using Xunit;

    public class ExceptionsDuringTransitions
    {
        public class CountingContext
        {
            public int Count { get; set; }
        }

        public abstract class BaseState : IState<CountingContext>
        {
            public virtual void OnEnter(CountingContext context)
            {
            }

            public virtual void OnExit(CountingContext context)
            {
            }
        }

        public class RegularState : BaseState
        {
            public override void OnExit(CountingContext context)
            {
                context.Count++;
            }
        }

        public class ThrowingState : BaseState
        {
            public override void OnEnter(CountingContext context)
            {
                throw new InvalidOperationException("Nope!");
            }
        }

        public class ExitThrowingState : BaseState
        {
            public override void OnExit(CountingContext context)
            {
                throw new InvalidOperationException("Nope!");
            }
        }


        [Fact(DisplayName = "Exception in 'OnEnter' hook will abort the state transition and move to previous state (if available)")]
        public void ExceptionInEnterHookWillAbortTransition()
        {
            var ctx = new CountingContext();
            var builder = new StateMachine<CountingContext, Trigger>.Builder(ctx);

            builder.State<RegularState>().IsInitialState().On(Trigger.MoveNext).GoesTo<ThrowingState>();

            var machine = builder.Build();
            Assert.Throws<InvalidOperationException>(() => machine.Trigger(Trigger.MoveNext));
            Assert.Equal(1, ctx.Count);

            // State machine should be back in RegularState now. MoveNext should be available, 'OnExit' will fire again.
            Assert.Throws<InvalidOperationException>(() => machine.Trigger(Trigger.MoveNext));
            Assert.Equal(2, ctx.Count);
        }

        [Fact(DisplayName = "Exception in 'OnEnter' hook will abort the state transition and stay in current state")]
        public void ExceptionInExitHookWillAbortTransition()
        {
            var ctx = new CountingContext();
            var builder = new StateMachine<CountingContext, Trigger>.Builder(ctx);

            builder.State<ExitThrowingState>().IsInitialState().On(Trigger.MoveNext).GoesTo<RegularState>();

            var machine = builder.Build();
            Assert.Throws<InvalidOperationException>(() => machine.Trigger(Trigger.MoveNext));

            // State machine should be back in ExitThrowingState now. MoveNext should be available, 'OnExit' will fire (and crash) again.
            Assert.Throws<InvalidOperationException>(() => machine.Trigger(Trigger.MoveNext));
        }

        [Fact(DisplayName = "If the initial state throws in OnEnter hook, it will abort state machine creation")]
        public void ThrowOnInitialEntryWillAbortMachineCreation()
        {
            var ctx = new CountingContext();
            var builder = new StateMachine<CountingContext, Trigger>.Builder(ctx);

            builder.State<ThrowingState>().IsInitialState().On(Trigger.MoveNext).GoesTo<RegularState>();

            Assert.Throws<InvalidOperationException>(() => builder.Build());
        }
    }
}
