﻿namespace StateMachine.Tests.Sync
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Exceptions;
    using Xunit;

    public enum Action
    {
        Assign,
        MarkAsComplete,
        Reject
    }

    public class BasicFunctionality
    {
        public class Draft : IState<object>
        {
            public void OnEnter(object context)
            {
            }

            public void OnExit(object context)
            {
            }
        }

        public class InProgress : IState<object>
        {
            public void OnEnter(object context)
            {
            }

            public void OnExit(object context)
            {
            }
        }

        public class Complete : IState<object>
        {
            public void OnEnter(object context)
            {
            }

            public void OnExit(object context)
            {
            }
        }

        private readonly StateMachine<object, Action>.Builder builder;
        public BasicFunctionality()
        {
            builder = new StateMachine<object, Action>.Builder();

            builder.State<Draft>()
                   .IsInitialState()
                   .On(Action.Assign)
                   .GoesTo<InProgress>();

            builder.State<InProgress>()
                   .On(Action.MarkAsComplete)
                   .GoesTo<Complete>()
                   .On(Action.Reject)
                   .GoesTo<Draft>();
        }

        [Fact]
        public void RegularUseCase()
        {
            var machine = builder.Build();
            var list = new List<Tuple<Type, Type>>();
            machine.TransitionCompleted += (sender, args) => list.Add(Tuple.Create(args.Source, args.Target));
            machine.Trigger(Action.Assign);
            machine.Trigger(Action.Reject);
            machine.Trigger(Action.Assign);
            machine.Trigger(Action.MarkAsComplete);
            Assert.Equal(4, list.Count);
            Assert.True(list.SequenceEqual(new[]
            {
                Tuple.Create(typeof (Draft), typeof (InProgress)),
                Tuple.Create(typeof (InProgress), typeof (Draft)),
                Tuple.Create(typeof (Draft), typeof (InProgress)),
                Tuple.Create(typeof (InProgress), typeof (Complete))
            }));
        }

        [Fact(DisplayName = "Event should be raised when the transition is complete")]
        public void EventIsRaisedWhenTransitionIsComplete()
        {
            bool eventRaised = false;
            var machine = builder.Build();
            machine.TransitionCompleted += (sender, args) => eventRaised = true;
            machine.Trigger(Action.Assign);
            Assert.True(eventRaised);
        }

        [Fact(DisplayName = "If there is no custom handler set for invalid trigger, exception will be thrown.")]
        public void ExceptionIsThrownWhenTheTriggerIsNotValidAndActionIsNotSet()
        {
            var machine = builder.Build();

            Assert.Throws<ExecutionException>(() => machine.Trigger(Action.Reject));
        }
    }
}
