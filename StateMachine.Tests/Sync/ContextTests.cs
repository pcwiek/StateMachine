﻿namespace StateMachine.Tests.Sync
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Xunit;

    public class ContextTests
    {
        public interface IIdentity<out T>
        {
            T Id { get; }
        }

        public class Start : IState<IIdentity<string>>
        {
            /// <summary>
            /// Called by the state machine when the state is entered to.
            /// </summary>
            /// <param name="context">Arbitrary context object passed from the state machine.</param>
            public void OnEnter(IIdentity<string> context)
            {
            }

            /// <summary>
            /// called by the state machine when the state is exited from.
            /// </summary>
            /// <param name="context">Arbitrary context object passed from the state machine.</param>
            public void OnExit(IIdentity<string> context)
            {
            }
        }

        public class End : IState<IIdentity<object>>
        {
            /// <summary>
            /// Called by the state machine when the state is entered to.
            /// </summary>
            /// <param name="context">Arbitrary context object passed from the state machine.</param>
            public void OnEnter(IIdentity<object> context)
            {
            }

            /// <summary>
            /// called by the state machine when the state is exited from.
            /// </summary>
            /// <param name="context">Arbitrary context object passed from the state machine.</param>
            public void OnExit(IIdentity<object> context)
            {
            }
        }

        public class SimpleStart : IState<IList<string>>
        {
            public void OnEnter(IList<string> context)
            {
                context.Add("Enter: " + typeof(SimpleStart).Name);
            }

            public void OnExit(IList<string> context)
            {
                context.Add("Exit: " + typeof(SimpleStart).Name);
            }
        }

        public class SimpleEnd : IState<IList<string>>
        {
            public void OnEnter(IList<string> context)
            {
                context.Add("Enter: " + typeof(SimpleEnd).Name);
            }

            public void OnExit(IList<string> context)
            {
                context.Add("Exit: " + typeof(SimpleEnd).Name);
            }
        }

        [Fact(DisplayName = "Custom context instance can be passed to state machine")]
        public void PassingCustomContext()
        {
            var list = new List<string>();
            var builder = new StateMachine<IList<string>, Trigger>.Builder(list);

            builder.State<SimpleStart>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<SimpleEnd>();

            builder.State<SimpleEnd>()
                   .On(Trigger.Loop)
                   .GoesTo<SimpleEnd>();

            var sm = builder.Build();
            sm.Trigger(Trigger.MoveNext);
            sm.Trigger(Trigger.Loop);

            // There's no last 'exit' from the last state, because it's... Last state. So, as name might suggest, there's no real exit from it.
            Assert.True(list.SequenceEqual(new[]
				                                 {
					                                 "Enter: SimpleStart",
													 "Exit: SimpleStart",
													 "Enter: SimpleEnd",
													 "Exit: SimpleEnd",
													 "Enter: SimpleEnd"
				                                 }));
        }

        [Fact(DisplayName = "If no context is specified, it will have the default value for the type")]
        public void ContextIsImplicitlyNull()
        {
            var builder = new StateMachine<IList<string>, Trigger>.Builder();

            builder.State<SimpleStart>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<SimpleEnd>();

            // Context is used on first transition, should fail here.
            Assert.Throws<NullReferenceException>(() => builder.Build());
        }

        [Fact(DisplayName = "States are covariant in context type parameter")]
        public void CovarianceTest()
        {
            var builder = new StateMachine<IIdentity<string>, Trigger>.Builder();
            builder.State<Start>()
                   .IsInitialState()
                   .On(Trigger.MoveNext)
                   .GoesTo<End>()
                   .On(Trigger.Loop)
                   .GoesTo<Start>();

            var sm = builder.Build();
            var transitions = 0;
            sm.TransitionCompleted += (sender, args) => transitions++;
            sm.Trigger(Trigger.Loop);
            sm.Trigger(Trigger.MoveNext);
            Assert.Equal(2, transitions);
        }
    }
}
