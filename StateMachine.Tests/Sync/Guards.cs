﻿namespace StateMachine.Tests.Sync
{
    using Xunit;

    public class Guards
    {
        public class Counter
        {
            public int Count { get; set; }
        }

        public class Start : IState<Counter>
        {
            public void OnEnter(Counter context)
            {
                context.Count++;
            }

            public void OnExit(Counter context)
            {
            }
        }

        public class End : IState<Counter>
        {
            public void OnEnter(Counter context)
            {
            }

            public void OnExit(Counter context)
            {
            }
        }

        [Fact(DisplayName = "If the guard evaluates to 'false', transition should not be triggered")]
        public void TransitionIsNotTriggeredIfGuardEvaluatesToFalse()
        {
            var ctx = new Counter();
            int localCount = 0;
            var builder = new StateMachine<Counter, Trigger>.Builder(ctx);
            builder.State<Start>()
                .IsInitialState()
                .On(Trigger.MoveNext, counter => counter.Count > 4)
                .GoesTo<End>()
                .On(Trigger.Loop)
                .GoesTo<Start>();

            var machine = builder.Build();
            machine.TransitionCompleted += (sender, args) => localCount++;
            machine.Trigger(Trigger.MoveNext);

            Assert.Equal(0, localCount);
        }

        [Fact(DisplayName = "If the guard evaluates to 'true', transition should be triggered")]
        public void TransitionIsTriggeredIfGuardEvaluatesToTrue()
        {
            var ctx = new Counter();
            int localCount = 0;
            var builder = new StateMachine<Counter, Trigger>.Builder(ctx);
            builder.State<Start>()
                .IsInitialState()
                .On(Trigger.MoveNext, counter => counter.Count > 4)
                .GoesTo<End>()
                .On(Trigger.Loop)
                .GoesTo<Start>();

            var machine = builder.Build();
            machine.TransitionCompleted += (sender, args) => localCount++;
            machine.Trigger(Trigger.MoveNext);
            
            Assert.Equal(0, localCount);

            ctx.Count = 10;
            machine.Trigger(Trigger.MoveNext);

            Assert.Equal(1, localCount);
        }
    }
}
