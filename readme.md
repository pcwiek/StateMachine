# State machine library

An implementation of finite state machine, based conceptually on [solid-state](http://code.google.com/p/solid-state/) (especially the state machine builder API) and [stateless](https://github.com/nblumhardt/stateless), but with a different top-level API and with support for async operations.

Contains both regular (via `StateMachine`) and async-friendly version (via `AsyncStateMachine`).

Also supports resuming from a given state type, which is useful if the context and the current state are stored externally, for example in a database.

## Quick example

### Regular version
		
	public enum Action
	{
		Approve,
		MoveBackToDraft,
		Complete
	}
	
	// Base class, just so the dummy implementation doesn't have to be repeated
	public class BaseState : IState<object>
	{
		public void OnEnter(object context)
		{	
		}

		public void OnExit(object context)
		{
		}
	}

	public class Draft : BaseState { }
	public class InProgress : BaseState { }
	public class Completed : BaseState { }


	var builder = new StateMachine<object, Action>.Builder()
	{ 
		OnInvalidTrigger = 
			(type, trigger) => Console.WriteLine("Type {0} does not have trigger {1} specified", type, trigger")
	};

	builder.State<Draft>()
		   .IsInitialState()
		   .On(Action.Approve)
		   .GoesTo<InProgress>();

	builder.State<InProgress>()
		   .On(Action.MoveBackToDraft)
		   .GoesTo<Draft>()
		   .On(Action.Complete)
		   .GoesTo<Completed>();

	var stateMachine = builder.Build();

	// Moves the state from Draft to InProgress
	stateMachine.Trigger(Action.Approve);

	// Moves the state from InProgress to Draft
	stateMachine.Trigger(Action.MoveBackToDraft);

	// Invalid trigger, will use the handler above (console print)
	stateMachine.Trigger(Action.Complete);

### Async version

	public enum Action
	{
		Approve,
		MoveBackToDraft,
		Complete
	}
	
	// Base class, just so the dummy implementation doesn't have to be repeated
	public class BaseState : IAsyncState<object>
	{
		public Task OnEnterAsync(object context)
		{	
			// Dummy implementation
			return Task.FromResult(false);
		}

		public Task OnExitAsync(object context)
		{
			// Dummy implementation
			return Task.FromResult(false);
		}
	}

	public class Draft : BaseState { }
	public class InProgress : BaseState { }
	public class Completed : BaseState { }


	var builder = new AsyncStateMachine<object, Action>.Builder() 
	{ 
		OnInvalidTrigger = 
			(type, trigger) => Console.WriteLine("Type {0} does not have trigger {1} specified", type, trigger")
	};

	builder.State<Draft>()
		   .IsInitialState()
		   .On(Action.Approve)
		   .GoesTo<InProgress>();

	builder.State<InProgress>()
		   .On(Action.MoveBackToDraft)
		   .GoesTo<Draft>()
		   .On(Action.Complete)
		   .GoesTo<Completed>();

	var stateMachine = await builder.BuildAsync();

	// Moves the state from Draft to InProgress
	await stateMachine.TriggerAsync(Action.Approve);

	// Moves the state from InProgress to Draft
	await stateMachine.TriggerAsync(Action.MoveBackToDraft);

	// Invalid trigger, will use the handler above (console print)
	await stateMachine.TriggerAsync(Action.Complete);


### TODO
Documentation... Definitely documentation.

###License
[MIT License](https://github.com/pcwiek/StateMachine/blob/master/LICENSE)